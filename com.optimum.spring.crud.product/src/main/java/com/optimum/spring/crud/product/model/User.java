package com.optimum.spring.crud.product.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data				// generate getters, setters, hashCode and toString
@NoArgsConstructor  // creates default constructor with no arguments
@AllArgsConstructor // creates constructors with all the arguments
@Builder			// Builder pattern will be implemented
public class User {

	private String userID;
	private String userPassword;
	
}
