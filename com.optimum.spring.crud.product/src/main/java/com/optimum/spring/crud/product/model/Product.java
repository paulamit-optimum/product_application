package com.optimum.spring.crud.product.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data				// generate getters, setters, hashCode and toString
//@NoArgsConstructor  // creates default constructor with no arguments
@AllArgsConstructor // creates constructors with all the arguments
@Builder			// Builder pattern will be implemented
//@Table(name = "products")
//@Component
public class Product {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long productID;
	
	//@NotNull(message = "Please add Product Name")
	//@Column(name="product_Name", nullable = false)
	private String productName;
	
	//@Column(name="product_Code", nullable = false)
	private String productCode;

	private Product() {
		
	}
	/*
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Product(Long productID, String productName, String productCode) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.productCode = productCode;
	}
	public Long getProductID() {
		return productID;
	}
	public void setProductID(Long productID) {
		this.productID = productID;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Override
	public String toString() {
		return "Product [productID=" + productID + ", productName=" + productName + ", productCode=" + productCode
				+ "]";
	}
	
	*/

}
