package com.optimum.spring.crud.product.service;

import java.util.List;

import com.optimum.spring.crud.product.error.ProductNotFoundException;
import com.optimum.spring.crud.product.model.Product;

public interface ProductService {

	public Product saveProduct(Product refProduct);

	public List getProducts();

	public Product getProductById(Long departmentID) throws ProductNotFoundException;

	public void deleteProductById(Long departmentID);

	public Product updateDepartment(Long productID, Product refProduct);

	public Product getProductByName(String productName);

}
