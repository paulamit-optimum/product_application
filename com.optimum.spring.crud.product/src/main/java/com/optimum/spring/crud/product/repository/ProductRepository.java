package com.optimum.spring.crud.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.optimum.spring.crud.product.model.Product;

@Repository
public interface ProductRepository  extends JpaRepository<Product,Long>{
	
	
	public Product getProductByProductName(String productName);

	public Object findByProductNameIgnoreCase(String string);
	
}
