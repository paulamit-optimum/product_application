package com.optimum.spring.crud.product.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.optimum.spring.crud.product.error.ProductNotFoundException;
import com.optimum.spring.crud.product.model.Product;
import com.optimum.spring.crud.product.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService{

	private ProductServiceImpl(){
		
	}
	
	@Autowired
	private ProductRepository refProductRepository;
	
	@Override
	public Product saveProduct(Product refProduct) {
		return refProductRepository.save(refProduct);
	}

	@Override
	public List getProducts() {
		return refProductRepository.findAll();
	}

	@Override
	public Product getProductById(Long departmentID) throws ProductNotFoundException {
		// return refProductRepository.findById(departmentID).get();
		
		// Handling Exception
		Optional<Product> refProduct = refProductRepository.findById(departmentID);
		if(!refProduct.isPresent()) {
			throw new ProductNotFoundException("Product Not Available..");
		}
		
		return refProduct.get();
	}

	@Override
	public void deleteProductById(Long departmentID) {
		refProductRepository.deleteById(departmentID);
	}

	@Override
	public Product updateDepartment(Long productID, Product product) {
		
		Product refproduct = refProductRepository.findById(productID).get();
		
		if(Objects.nonNull(product.getProductName()) &&! "".equalsIgnoreCase(product.getProductName())) {
			refproduct.setProductName(product.getProductName());
		}
		
		if(Objects.nonNull(product.getProductCode()) &&! "".equalsIgnoreCase(product.getProductCode())) {
			refproduct.setProductName(product.getProductCode());
		}
		
		return refProductRepository.save(refproduct);
	}

	@Override
	public Product getProductByName(String productName) {
		return refProductRepository.getProductByProductName(productName);
	}
	
	
}
