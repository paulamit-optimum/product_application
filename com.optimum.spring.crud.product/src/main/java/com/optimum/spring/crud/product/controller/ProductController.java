package com.optimum.spring.crud.product.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.optimum.spring.crud.product.error.ProductNotFoundException;
import com.optimum.spring.crud.product.model.Product;
import com.optimum.spring.crud.product.service.ProductService;

import javax.validation.Valid;

@RestController
@Validated
public class ProductController {
	
	private final Logger refLogger = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	private ProductService refProductService;
	
	@PostMapping("/products/add")
	public Product saveProduct(@Valid @RequestBody Product refProduct) {
		refLogger.info("inside saveProduct of ProductController");
		return refProductService.saveProduct(refProduct);
	}
	
	@GetMapping("/products")
	public List getAllProducts() {
		refLogger.info("inside getAllProducts of ProductController");
		return refProductService.getProducts();
	}
	
	@GetMapping("/products/{id}")
	public Product getProductById(@PathVariable("id") Long productID) throws ProductNotFoundException {
		return refProductService.getProductById(productID);
	}
	
	@GetMapping("/products/name/{name}")
	public Product getproductbyName(@PathVariable("name")  String productName) {
		return refProductService.getProductByName(productName);
	}
	
	@DeleteMapping("products/{id}")
	public String deleteProductById(@PathVariable("id") Long productID) {
		refProductService.deleteProductById(productID);
		return "Product Deleted Successfully..";
	}
	
	@PutMapping("products/{id}")
	public Product updateProduct(@PathVariable("id") Long productID, @RequestBody Product refProduct) {
		return refProductService.updateDepartment(productID,refProduct);
	}
}
