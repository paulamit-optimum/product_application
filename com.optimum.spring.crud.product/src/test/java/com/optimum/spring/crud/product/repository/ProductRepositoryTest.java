package com.optimum.spring.crud.product.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.optimum.spring.crud.product.model.Product;

@DataJpaTest
public class ProductRepositoryTest {
	
	@Autowired
	private ProductRepository refProductRepository;
	
	@Autowired
	private TestEntityManager refEntityManager;
	
	@BeforeEach
	void setUp() {
		Product refProduct = Product.builder()
				.productName("Lenevo - ThinkPad")
				.productCode("LT-001")
				.build();
		refEntityManager.persist(refProduct);
		// refProductRepository.save(refProduct);
		Assertions.assertThat(refProduct.getProductID()).isGreaterThan(0);
	}

	@Test
	public void WhenFindByID_thenReturnProduct() {
		Product refProduct = refProductRepository.findById(1L).get();
		assertEquals(refProduct.getProductName(), "Lenevo - ThinkPad");
		
	}
}



