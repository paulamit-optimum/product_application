package com.optimum.spring.crud.product.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.optimum.spring.crud.product.model.Product;
import com.optimum.spring.crud.product.service.ProductService;

@WebMvcTest(ProductController.class)
public class ProductControllerTest {
	
	@Autowired
	private MockMvc refMockMvc;
	
	@MockBean
	private ProductService refProductService;
	
	private Product refProduct;
	
	@BeforeEach
	void setUp() {
		refProduct = Product.builder()
				.productCode("Hikari")
				.productName("Datasource")
				.productID(1L)
				.build();
	}
	
	@Test
	void saveProduct() {
		Product refSaveProduct = Product.builder()
				.productCode("Hikari")
				.productName("Datasource")
				.build();
		
		Mockito.when(refProductService.saveProduct(refSaveProduct))
			.thenReturn(refProduct);
	
	//	refMockMvc.perform(MockMvcRequestBuilders.post("")
	//			.contentType(MediaType.APPLICATION_JSON)
	//			.content(null))
	}
	
	

}
