package com.optimum.spring.crud.product.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.optimum.spring.crud.product.model.Product;
import com.optimum.spring.crud.product.repository.ProductRepository;

@SpringBootTest
public class ProductServiceTest {

	@Autowired
	private ProductService refProductService;
	
	@MockBean
	private ProductRepository refProductRepository;
	
	@BeforeEach 
	void setUp() {
		Product refproduct = Product.builder()
				.productCode("101")
				.productName("iPhone")
				.productID(1L)
				.build();
		Mockito.when(refProductRepository.getProductByProductName("iPhone"))
				.thenReturn(refproduct);
		System.out.println("line 34 : "+refproduct.getProductName());
	}
	
	@Test
	@DisplayName("Get Data based on Valid Product Name")
	public void testCase1() { // when user search for valid product name then product should found
		System.out.println("inside testCase1()..");
		String productName = "iPhone";
		Product refData = refProductService.getProductByName(productName);
		System.out.println("line 43 "+refData.getProductName()); // null
		assertEquals(productName,refData.getProductName()); // true
	}
}
